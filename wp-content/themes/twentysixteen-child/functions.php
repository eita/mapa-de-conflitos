<?php
function my_theme_enqueue_styles() {

    $parent_style = 'twentysixteen-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

    wp_enqueue_style( 'leaflet', 'https://unpkg.com/leaflet@1.3.4/dist/leaflet.css');
    wp_enqueue_script( 'leaflet-js', 'https://unpkg.com/leaflet@1.3.4/dist/leaflet.js');

}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

#add columns
add_filter( 'manage_conflito_posts_columns', 'mapadeconflitos_admin_columns');
function mapadeconflitos_admin_columns( $columns ) {
  unset($columns['date']);
	return array_merge($columns, array(
    'cod' => __('Código'),
    'post_modified' => __('Modificado'),
    'modified_author' => __('Modificado por'),
    'localizacao' => __('Localização')
    )
  );
}

#add content
add_action( 'manage_conflito_posts_custom_column', 'mapadeconflitos_admin_columns_pop');
function mapadeconflitos_admin_columns_pop( $column_name ) {
   switch( $column_name ) {
      case 'cod':
         echo '<div id="cod-' . get_the_ID() . '">' . get_post_meta( get_the_ID(), 'cod', true ) . '</div>';
         break;
      case 'post_modified':
        column_mod_date( $post );
        break;
      case 'modified_author':
        the_modified_author();
        break;
      case 'localizacao':
        echo '<div>' . get_post_meta( get_the_ID(), 'lat', true ) . " - " . get_post_meta( get_the_ID(), 'lng', true ) . '</div>';
        break;
      }
}

#make sortable
add_filter( 'manage_edit-conflito_sortable_columns', 'mapadeconflitos_admin_columns_sortable' );
function mapadeconflitos_admin_columns_sortable( $sortable_columns ) {
	$sortable_columns[ 'cod' ] = 'cod';
  $sortable_columns[ 'post_modified' ] = 'post_modified';
  $sortable_columns[ 'modified_author' ] = 'modified_author';
	return $sortable_columns;
}

add_action( 'pre_get_posts', 'mapadeconflitos_custom_orderby' );

function mapadeconflitos_custom_orderby( $query ) {
  if ( ! is_admin() )
    return;

  $orderby = $query->get( 'orderby');

  if ( 'cod' == $orderby ) {
    $query->set( 'meta_key', 'cod' );
    $query->set( 'orderby', 'meta_value_num' );
  }
}

function column_mod_date( $post ) {

    global $post;
    global $mode;

		if ( '0000-00-00 00:00:00' === $post->post_modified ) {
			$t_time = $h_time = __( 'Unpublished' );
			$time_diff = 0;
		} else {
			$t_time = get_the_modified_time( __( 'Y/m/d g:i:s a' ) );
			$m_time = $post->post_modified;
			$time = get_post_modified_time( 'G', true, $post );

			$time_diff = time() - $time;

			if ( $time_diff > 0 && $time_diff < DAY_IN_SECONDS ) {
				$h_time = sprintf( __( '%s ago' ), human_time_diff( $time ) );
			} else {
				$h_time = mysql2date( __( 'Y/m/d' ), $m_time );
			}
		}

		if ( 'publish' === $post->post_status ) {
			$status = __( 'Published' );
		} elseif ( 'future' === $post->post_status ) {
			if ( $time_diff > 0 ) {
				$status = '<strong class="error-message">' . __( 'Missed schedule' ) . '</strong>';
			} else {
				$status = __( 'Scheduled' );
			}
		} else {
			$status = __( 'Last Modified' );
		}

		/**
		 * Filters the status text of the post.
		 *
		 * @since 4.8.0
		 *
		 * @param string  $status      The status text.
		 * @param WP_Post $post        Post object.
		 * @param string  $column_name The column name.
		 * @param string  $mode        The list display mode ('excerpt' or 'list').
		 */
		$status = apply_filters( 'post_date_column_status', $status, $post, 'post_modified', $mode );

		if ( $status ) {
			//echo $status . '<br />';
      echo "Modificado" . '<br />';

		}
		/** This filter is documented in wp-admin/includes/class-wp-posts-list-table.php */
		echo '<abbr title="' . $t_time . '">' . apply_filters( 'post_date_column_time', $h_time, $post, 'date', $mode ) . '</abbr>';

	}

### edit term meta for municipios_atingidos

function municipios_atingidos_add_custom_meta_field() {
        $fields = array(
          'codigo',
          'nome_munic_atin',
          'nome_uf_da_ficha',
          'codMun_atin',
          'sed_x',
          'sed_y',
          'cor_pree_y',
          'cor_pree_x',
          'atributo',
          'uf_do_geocod'
        );
        ?>
        <?php foreach ($fields as $field){ ?>
          <div class="form-field">
              <label for="term_meta[<?= $field ?>]"><?= $field ?></label>
              <input type="text" name="term_meta[<?= $field ?>]" id="term_meta[<?= $field ?>]" value="">
          </div>
        <?php } ?>
    <?php
    }
add_action( 'municipios_atingidos_add_form_fields', 'municipios_atingidos_add_custom_meta_field', 10, 2 );


function municipios_atingidos_edit_custom_meta_field($term) {

  $fields = array(
    'codigo',
    'nome_munic_atin',
    'nome_uf_da_ficha',
    'codMun_atin',
    'sed_x',
    'sed_y',
    'cor_pree_y',
    'cor_pree_x',
    'atributo',
    'uf_do_geocod'
  );

  $t_id = $term->term_id;
  $term_meta = get_term_meta( $t_id );
  ?>
  <?php foreach ($fields as $field){ ?>
    <tr class="form-field">
    <th scope="row" valign="top"><label for="term_meta[<?= $field ?>]"><?= $field ?></label></th>
        <td>
            <input type="text" name="term_meta[<?= $field ?>]" id="term_meta[<?= $field ?>]" value="<?php echo esc_attr( $term_meta[$field][0] ); ?>">
        </td>
    </tr>
  <?php } ?>
  <?php
}
add_action( 'municipios_atingidos_edit_form_fields','municipios_atingidos_edit_custom_meta_field', 10, 2 );

function mj_save_taxonomy_custom_meta_field( $term_id ) {
      if ( isset( $_POST['term_meta'] ) ) {
          $cat_keys = array_keys( $_POST['term_meta'] );
          foreach ( $cat_keys as $key ) {
              add_term_meta ($term_id, $key, $_POST['term_meta'][$key], false);
          }
      }
}

function mj_update_taxonomy_custom_meta_field( $term_id ) {
      if ( isset( $_POST['term_meta'] ) ) {
          $cat_keys = array_keys( $_POST['term_meta'] );
          foreach ( $cat_keys as $key ) {
              update_term_meta ($term_id, $key, $_POST['term_meta'][$key]);
          }
      }
}

add_action( 'edited_municipios_atingidos', 'mj_update_taxonomy_custom_meta_field', 10, 2 );
add_action( 'create_municipios_atingidos', 'mj_save_taxonomy_custom_meta_field', 10, 2 );
?>
