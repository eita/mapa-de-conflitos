<?php /* Template Name: Mapa */
	get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<?php echo do_shortcode( '[searchandfilter search_placeholder="escreva" taxonomies="search,populacao,atv_gerad,dano,impacto,uf" post_types="conflito" submit_label="Buscar"]' ); ?>
	<div id="mapid"></div>
		<ul>
		<?php
		// Start the loop.
		while ( have_posts() ) :
			the_post();
			$lat = get_post_meta(get_the_ID(),'lat', true);
			$lng = get_post_meta(get_the_ID(),'lng', true);
			$resumo = get_post_meta(get_the_ID(),"resumo");
			$points[] = array('title' => get_the_title(), 'lat' => $lat, 'lng' => $lng, 'resumo' => $resumo[0]);
			// Include the page content template.
			?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
			<?php

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>
		</ul>

	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>

<script>
	var mymap = L.map('mapid').setView([-10, -42], 3);

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(mymap);

	<?php
		foreach ($points as $p) {
			if ($p['lat']){
			echo '
				L.marker([' . $p['lat'] . ', ' . $p['lng'] . ']).addTo(mymap)
					.bindPopup("<b>' . $p['title'] . '</b><br />' . $p['resumo'] . '");
			';
			}
		}
	?>
</script>

<?php get_footer(); ?>
