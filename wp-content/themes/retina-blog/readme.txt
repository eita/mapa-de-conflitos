=== Retina Blog ===

Contributors: Thememattic
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready

Requires at least: 4.5
Tested up to: 4.9.5
Stable tag: 1.0.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A starter theme called Retina Blog.

== Description ==
Retina Blog is a easy-to-use Modern Clean WordPress theme developed specifically for personal, streamlined blog websites. Retina Blog theme has a perfect combination of modern, classic and minimal styles which will help you create a simple, clean and elegant blog. The theme features different layouts with sidebar position and color selection which you can pick and choose with a mouse click, allowing you to customize your WordPress site you want. Retina Blog is a perfect choice for you to create an remarkable blog of any purpose.


== License ==

Retina Blog WordPress Theme, Copyright (C) 2017, thememattic
Retina Blog is distributed under the terms of the GNU GPL

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Retina Blog includes support for Infinite Scroll in Jetpack.

------------------------------------------------------------------------------------------------------------------------
Simple line icons
Author: Sabbir http://simplelineicons.com
License Detail at: https://github.com/thesabbir/simple-line-icons

------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
jquery match height
Author: Liam liabru https://github.com/liabru
License Detail at: https://github.com/liabru/jquery-match-height
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
Magnific Popup
Author: Dmitry Semenov
License Detail at: http://dimsemenov.com/plugins/magnific-popup/
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
Slick.js
Author: Ken Wheeler
License Detail at: http://kenwheeler.github.io and http://github.com/kenwheeler/slick
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
BreadcrumbTrail:
Author: Justin Tadlock
Source: http://themehybrid.com/plugins/breadcrumb-trail
License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
theia-sticky-sidebar:
Author: Liviu Cristian Mirea Ghiban
Source: https://github.com/WeCodePixels/theia-sticky-sidebar/blob/master/LICENSE.txt
License: Licensed under the MIT license
------------------------------------------------------------------------------------------------------------------------

Source Sans Pro
Author: Paul D. Hunt
License Detail at: https://fonts.google.com/specimen/Source+Sans+Pro

------------------------------------------------------------------------------------------------------------------------
https://pixabay.com/en/dock-lake-finland-dark-evening-1365387/
https://pixabay.com/en/bare-beach-break-calm-chill-1985858/
------------------------------------------------------------------------------------------------------------------------

== Changelog ==

= 1.0.6- Nov 15 2018 =
* Fixed some issue as recomended and added slider number


= 1.0.4- Jun 05 2018 =
* Fixed some issue as recomended

= 1.0.4- Jun 05 2018 =
* update on fixing bug mentioned by reviewer

= 1.0.3- May 31 2018 =
* update on fixing

= 1.0.2- May 30 2018 =
* update on fixing

= 1.0.1- May 12 2018 =
* update on fixing

= 1.0 - May 25 2018 =
* Initial release

== Credits ==

* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
