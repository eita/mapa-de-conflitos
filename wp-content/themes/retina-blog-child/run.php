<?php /* Template Name: Run */
	get_header();
	ini_set('memory_limit', '-1');

	$fid = fopen ('municipios.csv','r');
	$row = fgetcsv ($fid);
	while ( $row = fgetcsv ($fid)) {
		$term = wp_insert_term( "$row[3] ($row[5])", 'municipio_atingido', $args = array() );
		if (is_wp_error( $term )) {
			if (isset($term->errors["term_exists"])) {
				continue;
			} else {
				echo "$row[3] ($row[5])"; var_dump($term);
				exit;
			}
		}
		add_term_meta ($term[term_id], 'cod_mun', $row[2], true);
		add_term_meta ($term[term_id], 'lat', $row[1], true);
		add_term_meta ($term[term_id], 'lng', $row[0], true);

		$args = array(
			'post_type' => 'conflito',
			'meta_query' => array(
				array(
	      'key' => 'cod_munic_p',
	      'value' => substr($row[2],0,6)
				)
			)
		);

		$the_query = new WP_Query( $args );
		 // The Loop
		 if ( $the_query->have_posts() ) {
		 	while ( $the_query->have_posts() ) {
		 		$the_query->the_post();
				wp_set_post_terms( get_the_ID(), $term[term_id], 'municipio_atingido', false );
		 	}
		 	wp_reset_postdata();
		 }
	}
?>
<?php get_footer(); ?>
