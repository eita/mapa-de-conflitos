<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Retina_Blog
 */

get_header();

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<div id="singlemap"></div>

<?php

	$municipio_principal = wp_get_post_terms( get_the_ID(), 'municipio_atingido' );
	$lat = get_term_meta( $municipio_principal[0]->term_id, 'lat', true );
	$lng = get_term_meta( $municipio_principal[0]->term_id, 'lng', true );
	$fonte = wp_get_post_terms( get_the_ID(), 'fonte' );
	$main_point = array('title' => get_the_title(), 'lat' => $lat, 'lng' => $lng, 'nome' => $municipio_principal[0]->name ,'fonte' => $fonte );


	$outros_municipios = wp_get_post_terms( get_the_ID(), 'outros_municipios' );
	$points[] = NULL;
	foreach ($outros_municipios as $term) {
		if ($municipio_principal[0]->name != $term->name){
			$lat = get_term_meta($term->term_id , 'lat' , True);
			$lng = get_term_meta($term->term_id , 'lng' , True);
			$nome = $term->name;
			$points[] = array('title' => $nome, 'lat' => $lat, 'lng' => $lng);
		}
	}

?>

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<script>
		var map = L.map('singlemap',{scrollWheelZoom : false}).setView([-14.5, -53], 4);

		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
			maxZoom: 18,
			attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
				'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			id: 'mapbox.streets'
		}).addTo(map);
		var markers = L.markerClusterGroup({
			maxClusterRadius: 10
		});
		<?php
		if ($points) {
			echo '
			myIcon = L.icon({
				iconUrl: "http://mapadeconflitos.eita.org.br/wp-content/uploads/2018/11/marker-icon-green.png",
			});';
			echo '
			otherSource = L.icon({
				iconUrl: "http://mapadeconflitos.eita.org.br/wp-content/uploads/2018/11/marker-icon-grey.png",
			});';
			foreach ($points as $p) {
				if ($p['lat'] != 0){
				echo '
					markers.addLayer(L.marker([' . $p['lat'] . ', ' . $p['lng'] . '], {icon: myIcon})
						.bindPopup("<b>' . $p['title'] . '</b>"));
				';
				}
			}
			echo "
				map.addLayer(markers);
			";
		}
		if ($main_point) {
			if ($main_point['lat']){
				if ($main_point['fonte']){
				echo '
					markers.addLayer(L.marker([' . $main_point['lat'] . ', ' . $main_point['lng'] . '], {icon: otherSource})
						.bindPopup("<b>' . $main_point['nome'] . '</b>"));
				';
				} else {
					echo '
						markers.addLayer(L.marker([' . $main_point['lat'] . ', ' . $main_point['lng'] . '])
							.bindPopup("<b>' . $main_point['nome'] . '</b>"));
					';
			}

			}
		}
		?>
	</script>

<?php
get_sidebar();
get_footer();
