<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Retina_Blog
 */

// get metadata
$ampliado = get_post_meta ( get_the_ID(), 'ampliado', 'true' );
$cronologia = get_post_meta ( get_the_ID(), 'cronologia', 'true' );
$fontes = get_post_meta ( get_the_ID(), 'fontes', 'true' );

$taxonomies = array(
  'UF' => 'uf',
  'Município Atingido' => 'municipio_atingido',
  'Outros Municípios' => 'outros_municipios',
  'População' => 'populacao',
  'Atividades Geradoras do Conflito' => 'atv_gerad',
  'Impactos Socioambientais' => 'impacto',
  'Danos à Saúde' => 'dano',
  'Fonte' => 'fonte'
);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-mh="article-group">
    <div class="article-wrapper">

        <header class="entry-header">
            <?php
                the_title('<h1 class="entry-title">', '</h1>');
            ?>
        </header><!-- .entry-header -->

        <div id="conflito_meta">
          <?php
            foreach ($taxonomies as $name => $taxonomy) {
              $terms = get_the_terms(get_the_ID(),$taxonomy);
              if ($terms) {
                $term_names = NULL;
                foreach ($terms as $term) {
                  if ($taxonomy == 'outros_municipios'){
                    $term_names[] = explode(' - ', $term->name)[0];
                  } else {
                    $term_names[] = $term->name;
                  }
                }
                echo "
                  <div class=\"entry-content\"><b>$name:</b> " . implode( ", ", $term_names) . "</div>
                ";
              }
            }
          ?>
        </div>

        <div id="conflito_content">
          <div class="entry-content">
            <h2>Síntese</h2>
            <?php the_content (); ?>
          </div>
          <?php if ($ampliado): ?>
          <div class="entry-content">
            <h2>Contexto Ampliado</h2>
            <?= apply_filters( 'the_content', $ampliado ); ?>
          </div>
          <?php endif ?>
          <?php if ($cronologia): ?>
          <div class="entry-content">
            <h2>Cronologia</h2>
            <?= apply_filters( 'the_content', $cronologia ); ?>
          </div>
          <?php endif ?>
          <?php if ($fontes): ?>
          <div class="entry-content">
            <h2>Fontes</h2>
            <?= apply_filters( 'the_content', $fontes ); ?>
          </div>
          <?php endif ?>
        </div>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
