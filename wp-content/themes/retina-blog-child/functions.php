<?php
function my_theme_enqueue_styles() {

    $parent_style = 'retina-blog-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

    wp_enqueue_script( 'retina-child-js', get_template_directory_uri() . '-child/js/script.js' );

    wp_enqueue_style( 'leaflet', 'https://unpkg.com/leaflet@1.3.4/dist/leaflet.css');
    wp_enqueue_script( 'leaflet-js', 'https://unpkg.com/leaflet@1.3.4/dist/leaflet.js');
    wp_enqueue_style( 'leaflet-cluster', 'https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css');
    wp_enqueue_style( 'leaflet-cluster-def', 'https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css');
    wp_enqueue_script( 'leaflet-cluster-js', 'https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js');

}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

#add columns
add_filter( 'manage_conflito_posts_columns', 'mapadeconflitos_admin_columns');
function mapadeconflitos_admin_columns( $columns ) {
  unset($columns['date']);
	return array_merge($columns, array(
    'post_modified' => __('Modificado'),
    'modified_author' => __('Modificado por'),
    )
  );
}

#add content
add_action( 'manage_conflito_posts_custom_column', 'mapadeconflitos_admin_columns_pop');
function mapadeconflitos_admin_columns_pop( $column_name ) {
   switch( $column_name ) {
      case 'post_modified':
        column_mod_date( $post );
        break;
      case 'modified_author':
        the_modified_author();
        break;
      }
}

#make sortable
add_filter( 'manage_edit-conflito_sortable_columns', 'mapadeconflitos_admin_columns_sortable' );
function mapadeconflitos_admin_columns_sortable( $sortable_columns ) {
  $sortable_columns[ 'post_modified' ] = 'post_modified';
  $sortable_columns[ 'modified_author' ] = 'modified_author';
	return $sortable_columns;
}

add_action( 'pre_get_posts', 'mapadeconflitos_custom_orderby' );

function mapadeconflitos_custom_orderby( $query ) {
  if ( ! is_admin() )
    return;

  $orderby = $query->get( 'orderby');

  if ( 'cod' == $orderby ) {
    $query->set( 'meta_key', 'cod' );
    $query->set( 'orderby', 'meta_value_num' );
  }
}

function column_mod_date( $post ) {

    global $post;
    global $mode;

		if ( '0000-00-00 00:00:00' === $post->post_modified ) {
			$t_time = $h_time = __( 'Unpublished' );
			$time_diff = 0;
		} else {
			$t_time = get_the_modified_time( __( 'Y/m/d g:i:s a' ) );
			$m_time = $post->post_modified;
			$time = get_post_modified_time( 'G', true, $post );

			$time_diff = time() - $time;

			if ( $time_diff > 0 && $time_diff < DAY_IN_SECONDS ) {
				$h_time = sprintf( __( '%s ago' ), human_time_diff( $time ) );
			} else {
				$h_time = mysql2date( __( 'Y/m/d' ), $m_time );
			}
		}

		if ( 'publish' === $post->post_status ) {
			$status = __( 'Published' );
		} elseif ( 'future' === $post->post_status ) {
			if ( $time_diff > 0 ) {
				$status = '<strong class="error-message">' . __( 'Missed schedule' ) . '</strong>';
			} else {
				$status = __( 'Scheduled' );
			}
		} else {
			$status = __( 'Last Modified' );
		}

		/**
		 * Filters the status text of the post.
		 *
		 * @since 4.8.0
		 *
		 * @param string  $status      The status text.
		 * @param WP_Post $post        Post object.
		 * @param string  $column_name The column name.
		 * @param string  $mode        The list display mode ('excerpt' or 'list').
		 */
		$status = apply_filters( 'post_date_column_status', $status, $post, 'post_modified', $mode );

		if ( $status ) {
			//echo $status . '<br />';
      echo "Modificado" . '<br />';

		}
		/** This filter is documented in wp-admin/includes/class-wp-posts-list-table.php */
		echo '<abbr title="' . $t_time . '">' . apply_filters( 'post_date_column_time', $h_time, $post, 'date', $mode ) . '</abbr>';

	}


?>
<?php
/**
 * Extend WordPress search to include custom fields
 *
 * https://adambalee.com
 */

/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );
