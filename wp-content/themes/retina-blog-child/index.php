<?php /* Template Name: Mapa */
	get_header();

  // Retrive Conflitos

	if (is_front_page()){
		$the_query = new WP_Query(
			array(
				'post_type' => 'conflito',
				'posts_per_page' => '-1'
			)
		);
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$municipio_principal = wp_get_post_terms( get_the_ID(), 'municipio_atingido' );
				$lat = get_term_meta( $municipio_principal[0]->term_id, 'lat', true );
				$lng = get_term_meta( $municipio_principal[0]->term_id, 'lng', true );
				$resumo = get_post_meta(get_the_ID(),"resumo");
				$fonte = wp_get_post_terms( get_the_ID(), 'fonte' );
				$points[] = array('title' => get_the_title(), 'lat' => $lat, 'lng' => $lng, 'resumo' => $resumo[0], 'link' => get_the_permalink(), 'fonte' => $fonte);
				// echo '<li><a href="' . the_permalink() . '">' . the_title() . '</a></li>';
			}
			/* Restore original Post Data */
			wp_reset_postdata();
		}
	} else {
		while ( have_posts() ) :

			the_post();
			$municipio_principal = wp_get_post_terms( get_the_ID(), 'municipio_atingido' );
			$lat = get_term_meta( $municipio_principal[0]->term_id, 'lat', true );
			$lng = get_term_meta( $municipio_principal[0]->term_id, 'lng', true );
			$resumo = get_post_meta(get_the_ID(),"resumo");
			$fonte = wp_get_post_terms( get_the_ID(), 'fonte' );
			$points[] = array('title' => get_the_title(), 'lat' => $lat, 'lng' => $lng, 'resumo' => $resumo[0], 'link' => get_the_permalink(), 'fonte' => $fonte);
			// echo '<li><a href="' . the_permalink() . '">' . the_title() . '</a></li>';
		endwhile;
	}
	$conflitos_count = $the_query->post_count > 0 ? $the_query->post_count : $wp_query->post_count;
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<?php
		echo do_shortcode( '[searchandfilter search_placeholder="Buscar no mapa" taxonomies="search,populacao,atv_gerad,dano,impacto,uf" post_types="conflito" submit_label="Filtrar"]' );
		echo "<span class='conflitos_count'>$conflitos_count conflito" . ($conflitos_count > 1 ? "s":"") . " encontrado" . ($conflitos_count > 1 ? "s":"") . " </span>";
	?>
	<div id="mainmap"></div>
	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<script>
	var mymap = L.map('mainmap',{scrollWheelZoom : false}).setView([-14.5, -53], 4);

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(mymap);
	var markers = L.markerClusterGroup({
		maxClusterRadius: 40
	});
	<?php
	if ($points) {
		echo '
			otherSource = L.icon({
				iconUrl: "http://mapadeconflitos.eita.org.br/wp-content/uploads/2018/11/marker-icon-grey.png",
			});';

		foreach ($points as $p) {
			if ($p['lat']){
				if ($p['fonte']) {
					echo '
						markers.addLayer(L.marker([' . $p['lat'] . ', ' . $p['lng'] . '], {icon: otherSource})
							.bindPopup("<b>' . $p['title'] . '</b><br />' . $p['resumo'] . '<br /><a href=\'' . $p['link'] . '\'>Ver mais</a>"));
					';
				} else {
					echo '
						markers.addLayer(L.marker([' . $p['lat'] . ', ' . $p['lng'] . '])
							.bindPopup("<b>' . $p['title'] . '</b><br />' . $p['resumo'] . '<br /><a href=\'' . $p['link'] . '\'>Ver mais</a>"));
					';
				}
			}
		}

		echo "mymap.addLayer(markers);";
	}
	?>
</script>

<?php get_footer(); ?>
